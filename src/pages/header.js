import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

export default function Header({ name }) {
    return (
        <View style={s.sectionContainer}>
            <View style={s.containerLogo}>
                <Text style={s.title}>{name}</Text>
            </View>
        </View>
    );
}

const s = StyleSheet.create({
    sectionContainer: {
        paddingTop: 32,
        paddingBottom: 10,
    },
    containerLogo: {
        display: "flex",
        justifyContent: "flex-start",
        alignItems: "center",
    },
    title: {
        fontSize: 24,
        color: "white",
        paddingTop: 10,
    }
})