import React, { useState, useContext } from 'react';
import { Image, TextInput, View, StyleSheet, Text, KeyboardAvoidingView, TouchableOpacity, TouchableHighlight, Dimensions, ImageBackground, Modal } from 'react-native';
import Menu from './menu'
import { ThemeContext } from '../component/utils/context/theme-context';

import imageBackground from '../img/background-image.jpg'
import imageBackgroundNight from '../img/background-image-night.jpg'

import search from '../component/utils/icon/search.png'
import locate from '../component/utils/icon/locate.png'

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default function Accueil({ navigation }) {
    const [city, setCity] = useState("");
    const [modalVisible, setModalVisible] = useState(false)
    const [context, setContext] = useContext(ThemeContext);

    function getLocalisation() {
        // On vérifie si la géolocalisation est active / autorisé, et on envoie les valeurs à la page.
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function (position) {
                navigation.navigate('Meteo', {
                    method: "loc",
                    city: "",
                    lat: position.coords.latitude,
                    lon: position.coords.longitude
                })
            })
        }
    }

    function getCity() {
        // On envoie ici la variable entrée dans le formulaire de recherche.
        if (city.length === 0) {
            setModalVisible(true);
        } else {
            navigation.navigate('Meteo', {
                method: "search",
                city: city,
                lat: "",
                lon: ""
            })
        }
    }

    return (
        <KeyboardAvoidingView behavior="position">
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Le modal a été fermé.");
                }}
            >
                <View style={s.centeredView}>
                    <View style={[{ backgroundColor: context.bgOthers }, s.modalView]}>
                        <Text style={[{ color: context.color }, s.modalText]}>Merci de remplir le champ Rechercher.</Text>

                        <TouchableHighlight
                            style={[{ backgroundColor: context.buttonHome }, s.openButton]}
                            onPress={() => {
                                setModalVisible(!modalVisible);
                            }}
                        >
                            <Text style={s.textStyle}>Fermer</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </Modal>
            <ImageBackground source={context.name === "Dark" ? imageBackgroundNight : imageBackground} style={s.image}>
                <View style={s.bloc_title}>
                    <Text style={s.title}>
                        Bienvenue sur votre {"\n"} Application Météo !
                    </Text>
                </View>
                <View style={[{backgroundColor: context.bgHome}, s.bloc_form]}>
                    <View style={s.form}>
                        <View style={s.bloc}>
                            <Text style={[{ color: context.colorHome }, s.subtitle]}>Recherchez votre ville</Text>
                        </View>
                        <View style={s.bloc}>
                            <TextInput
                                style={[{ color: context.colorSearchBoxHome, borderColor: context.searchBoxHome }, s.search_text]}
                                onChangeText={text => setCity(text)}
                                city={city}
                                placeholder="Recherchez votre ville ici..."
                                placeholderTextColor={context.colorHome}
                            />
                        </View>
                        <View style={s.bloc}>
                            <TouchableOpacity
                                onPress={(getCity)}
                                style={[{backgroundColor: context.buttonHome}, s.appButtonContainer]}
                            >
                                <Text style={s.appButtonText}>Recherchez</Text>
                                <Image source={search} style={s.icon} />
                            </TouchableOpacity>
                        </View>
                        <View style={s.bloc}>
                            <TouchableOpacity
                                onPress={(getLocalisation)}
                                style={[{backgroundColor: context.buttonHome}, s.appButtonContainer]}
                            >
                                <Text style={s.appButtonText}>Localisez-moi</Text>
                                <Image source={locate} style={s.icon} />
                            </TouchableOpacity>
                        </View>
                        <Text></Text>
                    </View>
                </View>
                <Menu />
            </ImageBackground>
        </KeyboardAvoidingView>
    );
}

const s = StyleSheet.create({
    image: {
        height: windowHeight,
        width: windowWidth,
        position: "relative",
    },
    bloc_title: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: 300,
    },
    title: {
        color: "white",
        fontSize: 30,
        fontWeight: "200",
    },
    bloc_form: {
        flex: 1,
        borderTopLeftRadius: 60,
        borderTopRightRadius: 60,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,
        elevation: 24,
        alignItems: "center",
        marginBottom: -50
    },
    form: {
        flex: 1,
        width: "90%",
        alignItems: "center",
        paddingTop: 30,
    },
    bloc: {
        width: "85%",
        height: "15%",
        alignItems: "center",
        justifyContent: "center",
        marginTop: 20,
    },
    subtitle: {
        fontSize: 20,
        fontWeight: "700",
    },
    search_text: {
        flex: 1,
        width: "100%",
        fontSize: 17,
        textAlign: 'center',
        borderWidth: 2,
        borderRadius: 10,
        fontWeight: "400",
        fontStyle: "italic",
    },
    appButtonContainer: {
        flex: 1,
        flexDirection: "row",
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        elevation: 8,
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 12,
    },
    appButtonText: {
        fontSize: 15,
        color: "#fff",
        fontWeight: "400",
        textTransform: "uppercase",
    },
    icon: {
        marginLeft: 10,
        width: 20,
        height: 20,
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    modalView: {
        margin: 20,
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        borderRadius: 10,
        padding: 10,
        marginTop: 15,
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center",
        fontWeight: "400",
        fontStyle: "italic"
    }
})