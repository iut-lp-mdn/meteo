import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';

import Connexion from './connexion';
import Inscription from './inscription';

const Root = createStackNavigator();

const RootNavigation = ({navigation}) => (
    <Root.Navigator headerMode='none'>
        <Root.Screen name="Connexion" component={Connexion}/>
        <Root.Screen name="Inscription" component={Inscription}/>
    </Root.Navigator>
);

export default RootNavigation;