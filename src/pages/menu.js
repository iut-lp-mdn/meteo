import React, { useContext } from 'react';
import { TouchableOpacity, StyleSheet, Image, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { ThemeContext } from '../component/utils/context/theme-context';

import burger from '../component/utils/icon/menu.png'

export default function Menu() {
    const navigation = useNavigation();
    const [context, setContext] = useContext(ThemeContext);

    return (
        <View style={s.shadow}>
            <TouchableOpacity style={[{backgroundColor: context.menu}, s.bloc]} onPress={() => navigation.openDrawer()}>
                <Image source={burger} style={s.image} />
            </TouchableOpacity>
        </View>
    );
}

const s = StyleSheet.create({
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
    },
    bloc: {
        position: "relative",
        bottom: 20,
        left: 20,
        height: 50,
        width: 50,
        borderRadius: 100,
        justifyContent: "center",
        alignItems: "center",
    },
    image: {
        height: 25,
        width: 25
    }
})