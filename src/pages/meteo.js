import React, { useContext } from 'react';
import { View, StyleSheet } from 'react-native';
import { ThemeContext } from '../component/utils/context/theme-context';

import Resume from '../component/resume'
import Menu from './menu'
import Error from "../component/error";
import Header from './header';

export default function City(props) {
    const [context, setContext] = useContext(ThemeContext);
    return (
        <View style={[{ backgroundColor: context.background }, s.blocList]}>
            <Header name="Météo" />
            {props.route.params === undefined ? // Si rien n'est envoyé, alors on renvoie une erreur.
                <Error value="void_city" />
                : props.route.params.method === "search" ? // Si c'est depuis le formulaire que l'on obtient la valeur, alors on envoie les données pour un affichage des données grâce au Nom de la ville (requête axios différente).
                    <View style={s.root}>
                        <Resume data={props.route.params} />
                    </View>
                    : // Sinon on envoie les données de géolocalisation.
                    <View style={s.root}>
                        <Resume data={props.route.params} />
                    </View>
            }
            <Menu />

        </View>
    );
}

const s = StyleSheet.create({
    blocList: {
        flex: 1,
        paddingTop: 15,
        position: "relative",
    },
    root: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "white",
        marginBottom: -50,
    }
})