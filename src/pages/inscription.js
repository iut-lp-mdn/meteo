import React, { useState, useContext } from 'react';
import {
    View,
    Text,
    Button,
    TouchableOpacity,
    Dimensions,
    TextInput,
    Platform,
    StyleSheet,
    ScrollView,
    StatusBar
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import { ThemeContext } from '../component/utils/context/theme-context';

const SignInScreen = ({ navigation }) => {
    const [context, setContext] = useContext(ThemeContext)
    const [data, setData] = useState({
        pseudo: '',
        password: '',
        confirm_password: '',
        check_textInputChange: false,
        secureTextEntry: true,
        confirm_secureTextEntry: true,
    });

    const textInputChange = (val) => {
        if (val.length !== 0) {
            setData({
                ...data,
                pseudo: val,
                check_textInputChange: true
            });
        } else {
            setData({
                ...data,
                pseudo: val,
                check_textInputChange: false
            });
        }
    }

    const handlePasswordChange = (val) => {
        setData({
            ...data,
            password: val
        });
    }

    const handleConfirmPasswordChange = (val) => {
        setData({
            ...data,
            confirm_password: val
        });
    }

    const updateSecureTextEntry = () => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        });
    }

    const updateConfirmSecureTextEntry = () => {
        setData({
            ...data,
            confirm_secureTextEntry: !data.confirm_secureTextEntry
        });
    }

    return (
        <View style={[{ backgroundColor: context.background }, s.container]}>
            <StatusBar backgroundColor='#33A5A6' barStyle="light-content" />

            <Animatable.View
                animation="fadeInUpBig"
                style={[{ backgroundColor: context.bgHome }, s.footer]}
            >
                <ScrollView>
                    <Text style={[{ color: context.colorHome }, s.text_footer]}>Nom d'utilisateur</Text>
                    <View style={s.action}>
                        <FontAwesome
                            name="user-o"
                            color={context.colorHome}
                            size={20}
                        />
                        <TextInput
                            placeholder="Votre nom d'utilisateur"
                            placeholderTextColor={context.colorHome}
                            style={[{ color: context.colorHome }, s.textInput]}
                            autoCapitalize="none"
                            onChangeText={(val) => textInputChange(val)}
                        />
                        {data.check_textInputChange ?
                            <Animatable.View
                                animation="bounceIn"
                            >
                                <Feather
                                    name="check-circle"
                                    color="green"
                                    size={20}
                                />
                            </Animatable.View>
                            : null}
                    </View>

                    <Text style={[{ color: context.colorHome, marginTop: 35 }, s.text_footer]}>Mot de passe</Text>
                    <View style={s.action}>
                        <Feather
                            name="lock"
                            color={context.colorHome}
                            size={20}
                        />
                        <TextInput
                            placeholder="Votre mot de passe"
                            placeholderTextColor={context.colorHome}
                            secureTextEntry={data.secureTextEntry ? true : false}
                            style={[{ color: context.colorHome }, s.textInput]}
                            autoCapitalize="none"
                            onChangeText={(val) => handlePasswordChange(val)}
                        />
                        <TouchableOpacity
                            onPress={updateSecureTextEntry}
                        >
                            {data.secureTextEntry ?
                                <Feather
                                    name="eye-off"
                                    color="grey"
                                    size={20}
                                />
                                :
                                <Feather
                                    name="eye"
                                    color="grey"
                                    size={20}
                                />
                            }
                        </TouchableOpacity>
                    </View>

                    <Text style={[{ color: context.colorHome, marginTop: 35 }, s.text_footer]}>Confirmer votre mot de passe</Text>
                    <View style={s.action}>
                        <Feather
                            name="lock"
                            color={context.colorHome}
                            size={20}
                        />
                        <TextInput
                            placeholder="Confirmer votre mot de passe"
                            placeholderTextColor={context.colorHome}
                            secureTextEntry={data.confirm_secureTextEntry ? true : false}
                            style={[{ color: context.colorHome }, s.textInput]}
                            autoCapitalize="none"
                            onChangeText={(val) => handleConfirmPasswordChange(val)}
                        />
                        <TouchableOpacity
                            onPress={updateConfirmSecureTextEntry}
                        >
                            {data.secureTextEntry ?
                                <Feather
                                    name="eye-off"
                                    color="grey"
                                    size={20}
                                />
                                :
                                <Feather
                                    name="eye"
                                    color="grey"
                                    size={20}
                                />
                            }
                        </TouchableOpacity>
                    </View>
                    <View style={s.textPrivate}>
                        <Text style={{color: context.colorHome}}>
                            En vous inscrivant vous acceptez nos
                        </Text>
                        <Text style={{color: context.colorHome, fontWeight: 'bold' }}>{" "} CGU</Text>
                    </View>
                    <View style={s.button}>
                        <TouchableOpacity
                            style={[{backgroundColor: context.buttonHome}, s.signIn]}
                            onPress={() => { }}
                        >
                            <Text style={[{color: "#FFFFFF"}, s.textSign]}>S'inscrire  </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => navigation.goBack()}
                            style={[{ color: context.colorSearchBoxHome, borderColor: context.searchBoxHome, borderWidth: 1, marginTop: 15 }, s.signIn]}
                        >
                            <Text style={[{color: context.colorHome}, s.textSign]}>Se connecter</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </Animatable.View>
        </View>
    );
};

export default SignInScreen;

const s = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    footer: {
        width: '90%',
        borderRadius: 10,
        padding: 30,
        margin: 30,
    },
    text_header: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 30
    },
    text_footer: {
        fontSize: 18
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
    },
    button: {
        alignItems: 'center',
        marginTop: 50
    },
    signIn: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    textSign: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    textPrivate: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 20
    }
});
