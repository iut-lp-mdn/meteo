import React, { useState, useContext } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Platform,
    StyleSheet,
    StatusBar,
    Modal,
    TouchableHighlight
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';

import { AuthContext, users as Users } from '../component/utils/context/user-context';
import { ThemeContext } from '../component/utils/context/theme-context';

export default function connexion({ navigation }) {

    const [data, setData] = useState({
        pseudo: '',
        password: '',
        check_textInputChange: false,
        secureTextEntry: true,
        isValidUser: true,
        isValidPassword: true,
    });

    const [modalVisible, setModalVisible] = useState(false)
    const [messages, setMessages] = useState('')
    const [context, setContext] = useContext(ThemeContext)

    const { signIn } = useContext(AuthContext);

    const textInputChange = (val) => {
        if (val.trim().length >= 4) {
            setData({
                ...data,
                pseudo: val,
                check_textInputChange: true,
                isValidUser: true
            });
        } else {
            setData({
                ...data,
                pseudo: val,
                check_textInputChange: false,
                isValidUser: false
            });
        }
    }

    const handlePasswordChange = (val) => {
        if (val.trim().length >= 8) {
            setData({
                ...data,
                password: val,
                isValidPassword: true
            });
        } else {
            setData({
                ...data,
                password: val,
                isValidPassword: false
            });
        }
    }

    const updateSecureTextEntry = () => {
        setData({
            ...data,
            secureTextEntry: !data.secureTextEntry
        });
    }

    const handleValidUser = (val) => {
        if (val.trim().length >= 4) {
            setData({
                ...data,
                isValidUser: true
            });
        } else {
            setData({
                ...data,
                isValidUser: false
            });
        }
    }

    const loginHandle = (pseudo, password) => {

        const foundUser = Users.filter(item => {
            return pseudo == item.pseudo && password == item.password;
        });

        if (data.pseudo.length == 0 || data.password.length == 0) {
            setModalVisible(true)
            setMessages("void")
            return;
        }

        if (foundUser.length == 0) {
            setModalVisible(true)
            setMessages("wrong")
            return;
        }
        signIn(foundUser);
    }

    return (
        <View style={[{ backgroundColor: context.background }, s.container]}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
            >
                <View style={s.centeredView}>
                    <View style={[{ backgroundColor: context.bgOthers }, s.modalView]}>
                        <Text style={[{ color: context.color }, s.modalText]}>{messages === "void" ? "Merci de remplir les deux champs." : "Pseudo ou mot de passe incorrect"}</Text>

                        <TouchableHighlight
                            style={[{ backgroundColor: context.buttonHome }, s.openButton]}
                            onPress={() => {
                                setModalVisible(!modalVisible);
                            }}
                        >
                            <Text style={s.textStyle}>Fermer</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </Modal>
            <StatusBar backgroundColor='#33A5A6' barStyle="light-content" />

            <Animatable.View
                animation="fadeInUpBig"
                style={[{backgroundColor: context.bgHome}, s.footer]}
            >
                <Text style={[{ color: context.colorHome }, s.text_footer]}>Pseudo</Text>
                <View style={s.action}>
                    <FontAwesome
                        name="user-o"
                        color={context.colorHome}
                        size={20}
                    />
                    <TextInput
                        placeholder="Votre pseudo"
                        placeholderTextColor={context.colorHome}
                        style={[{ color: context.colorHome }, s.textInput]}
                        autoCapitalize="none"
                        onChangeText={(val) => textInputChange(val)}
                        onEndEditing={(e) => handleValidUser(e.nativeEvent.text)}
                    />
                    {data.check_textInputChange ?
                        <Animatable.View
                            animation="bounceIn"
                        >
                            <Feather
                                name="check-circle"
                                color="green"
                                size={20}
                            />
                        </Animatable.View>
                        : null}
                </View>
                {data.isValidUser ? null :
                    <Animatable.View animation="fadeInLeft" duration={500}>
                        <Text style={s.errorMsg}>Username must be 4 characters long.</Text>
                    </Animatable.View>
                }


                <Text style={[{ color: context.colorHome, marginTop: 35 }, s.text_footer]}>Mot de passe</Text>
                <View style={s.action}>
                    <Feather
                        name="lock"
                        color={context.colorHome}
                        size={20}
                    />
                    <TextInput
                        placeholder="Votre mot de passe"
                        placeholderTextColor={context.colorHome}
                        secureTextEntry={data.secureTextEntry ? true : false}
                        style={[{ color: context.colorHome }, s.textInput]}
                        autoCapitalize="none"
                        onChangeText={(val) => handlePasswordChange(val)}
                    />
                    <TouchableOpacity
                        onPress={updateSecureTextEntry}
                    >
                        {data.secureTextEntry ?
                            <Feather
                                name="eye-off"
                                color="grey"
                                size={20}
                            />
                            :
                            <Feather
                                name="eye"
                                color="grey"
                                size={20}
                            />
                        }
                    </TouchableOpacity>
                </View>

                <TouchableOpacity>
                    <Text style={{ color: context.colorHome, marginTop: 15, textDecorationLine: "underline" }}>Mot de passe oublié ?</Text>
                </TouchableOpacity>
                <View style={s.button}>
                    <TouchableOpacity
                        style={[{backgroundColor: context.buttonHome}, s.signIn]}
                        onPress={() => { loginHandle(data.pseudo, data.password) }}
                    >

                        <Text style={[{color: "#FFFFFF"}, s.textSign]}>Se connecter</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => navigation.navigate('Inscription')}
                        style={[{ color: context.colorSearchBoxHome, borderColor: context.searchBoxHome, borderWidth: 1, marginTop: 15 }, s.signIn]}
                    >
                        <Text style={[{color: context.colorHome}, s.textSign]}>S'inscrire</Text>
                    </TouchableOpacity>
                </View>
            </Animatable.View>
        </View>
    );
};

const s = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    footer: {
        width: '90%',
        borderRadius: 10,
        padding: 30,
        margin: 30,
    },
    text_footer: {
        fontSize: 18
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    actionError: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#FF0000',
        paddingBottom: 5
    },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
    },
    errorMsg: {
        color: '#FF0000',
        fontSize: 14,
    },
    button: {
        alignItems: 'center',
        marginTop: 50
    },
    signIn: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    textSign: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    modalView: {
        margin: 20,
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        borderRadius: 10,
        padding: 10,
        marginTop: 15,
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
});
