import React from "react"
import { View, Text, StyleSheet, TouchableOpacity, Dimensions } from "react-native"
import { useNavigation } from '@react-navigation/native';

const windowHeight = Dimensions.get('window').height;

export default function Error(value) {
    const navigation = useNavigation();
    return (
        <View style={s.bloc}>
            <View style={s.root}>
                <Text style={s.oups}>Oups...</Text>
                {value.value === "404" ? // Renvoie le message d'erreur 404, si la ville n'est pas trouvée.
                    <View>
                        <Text style={s.advertise}>Nous ne trouvons pas votre ville.</Text>
                        <Text style={s.help}>Merci de vérifier l'orthographe</Text>
                        <Text style={s.help}>N'oubliez pas les caractères spéciaux</Text>
                        <Text style={s.help}>tel que : - '</Text>
                    </View>
                    : // Renvoie le message d'erreur, qui affiche que la personne n'a pas recherché de ville ou qu'elle ne s'est pas géolocaliser.
                    <Text style={s.advertise}>Vous semblez avoir oublié de rechercher une ville</Text>
                }
                <View style={s.bloc_button}>
                    <TouchableOpacity
                        onPress={() => navigation.navigate('Accueil')}
                        style={s.appButtonContainer}
                    >
                        <Text style={s.appButtonText}>Rechercher une ville</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}


const s = StyleSheet.create({
    bloc: {
        flex: 1,
    },
    root: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "white",
        marginBottom: -50,
        height: windowHeight,
    },
    advertise: {
        textAlign: "center",
        fontSize: 20,
        color: "#186184",
        marginTop: 20,
        marginHorizontal: 10,
        marginBottom: 20,
    },
    help: {
        textAlign: "center",
        fontSize: 15,
        color: "#186184",

        marginHorizontal: 10,
    },
    oups: {
        fontSize: 30,
        color: "#E54F5F"
    },
    appButtonContainer: {
        flex: 1,
        flexDirection: "row",
        width: "100%",
        alignItems: "center",
        elevation: 8,
        backgroundColor: "#186184",
        borderRadius: 10,
        marginTop: 10,
        paddingVertical: 10,
        paddingHorizontal: 12,
    },
    appButtonText: {
        fontSize: 15,
        color: "#fff",
        fontWeight: "400",
        textTransform: "uppercase",
    },
    bloc_button: {
        marginHorizontal: 30,
        height: "10%",
        marginTop: 20,
    }
})