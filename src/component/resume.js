import React, { useEffect, useState, useContext } from 'react';
import { Text, View, StyleSheet, Vibration, Image, ScrollView, Dimensions, TouchableHighlight, Modal, TouchableOpacity } from 'react-native';
import { Title } from 'react-native-paper';
import DropDownPicker from 'react-native-dropdown-picker';
import { ThemeContext } from './utils/context/theme-context';

import axios from 'axios';

import Week from "./week";
import Hourly from "./hourly";
import Stats from "./stats";
import Error from "./error";

import { degreeTab } from './utils/api/degree_api'

import params from "./utils/icon/params.png"

function Vibrate() {
  return Vibration.vibrate()
}

const windowWidth = Dimensions.get('window').width;

function strUcFirst(value) {
  return value.substr(0, 1).toUpperCase() + value.substr(1);
}

export default function Resume(props) {
  const [data, setData] = useState({});
  const [main, setMain] = useState({});
  const [weather, setWeather] = useState([]);
  const [icon, setIcon] = useState([]);
  const [coord, setCoord] = useState({});
  const [sys, setSys] = useState({});
  const [loadingData, setLoadingData] = useState(true);
  const [display, setDisplay] = useState("today");
  const [status, setStatus] = useState();
  const [degree, setDegree] = useState("metric")
  const [valueDegree, setValueDegree] = useState("C")
  const [modalVisible, setModalVisible] = useState(false)
  const [context, setContext] = useContext(ThemeContext);

  function chooseDisplay() {
    // Affichage des components en fonction du bouton sélectionner par l'utilisateur.
    if (display === "today")
      return <Hourly coord={coord} deg={degree} value={valueDegree} />
    if (display === "week")
      return <Week coord={coord} deg={degree} value={valueDegree} />
    if (display === "all") {
      return (
        <View>
          <Hourly coord={coord} deg={degree} value={valueDegree} />
          <Week coord={coord} deg={degree} value={valueDegree} />
        </View>
      )
    }
  }

  useEffect(() => {
    async function getData() {
      setLoadingData(true)
      setStatus()
      const result = props.data.method === "search" ? // Si la ville a été recherchée.
        await axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${props.data.city}&units=${degree}&lang=fr&appid=8c19af5d8320e75a3a5fbf562b144995`)
          .catch(function (error) { // Vérification que la ville existe, sinon renvoie une erreur 404.
            if (error) {
              setStatus(error.response.status)
              return;
            }
          })
        : await axios.get(`http://api.openweathermap.org/data/2.5/weather?lat=${props.data.lat}&lon=${props.data.lon}&lang=fr&units=${degree}&appid=8c19af5d8320e75a3a5fbf562b144995`) // Si c'est la géolocalisation qui est utilisée.
      setData(result.data)
      setMain(result.data.main)
      setCoord(result.data.coord)
      setWeather(result.data.weather)
      setIcon(result.data.weather.map(obj => obj.icon))
      setSys(result.data.sys)
      setLoadingData(false)
      result.data !== undefined && Vibrate()
    }
    getData()
  }, [props, degree]);

  return (
    <>
      {status === 404 ? <Error value="404" /> :
        <ScrollView style={[{ backgroundColor: context.background }, s.root]}>
          <View>
            {loadingData ?
              <View style={s.loading}>
                <Text style={[{ color: context.color2 }, s.loading_txt]}>Chargement de votre Météo ...</Text>
              </View>
              :
              <View>
                <Modal
                  animationType="slide"
                  transparent={true}
                  visible={modalVisible}
                  onRequestClose={() => {
                    Alert.alert("Le modal a été fermé.");
                  }}
                >
                  <View style={s.centeredView}>
                    <View style={[{ backgroundColor: context.bgOthers }, s.modalView]}>
                      <Text style={[{ color: context.color }, s.modalText]}>Choisissez votre valeur de degré.</Text>
                      <View style={s.selector}>
                        <DropDownPicker
                          items={degreeTab.map(item => item)}
                          defaultValue={degree}
                          containerStyle={s.containerStyle}
                          style={{ backgroundColor: context.bgWeekly, borderColor: context.color2 }}
                          itemStyle={s.itemStyle}
                          dropDownStyle={{ backgroundColor: context.bgWeekly, borderColor: context.color2 }}
                          labelStyle={[{ color: context.color2 }, s.labelStyle]}
                          onChangeItem={(item) => {
                            setDegree(item.value)
                            setValueDegree(item.label.charAt(0))
                            setModalVisible(!modalVisible)
                          }}
                          arrowColor={context.color2}
                        />
                      </View>

                      <TouchableHighlight
                        style={[{ backgroundColor: context.buttonHome }, s.openButton]}
                        onPress={() => {
                          setModalVisible(!modalVisible);
                        }}
                      >
                        <Text style={s.textStyle}>Fermer</Text>
                      </TouchableHighlight>
                    </View>
                  </View>
                </Modal>
                <View style={s.bloc_header}>
                  <TouchableHighlight style={s.params} onPress={() => {
                    setModalVisible(!modalVisible);
                  }}>
                    <Image source={params} style={s.image} />
                  </TouchableHighlight>
                  <Image
                    style={s.tinyLogo}
                    source={{
                      uri: `http://openweathermap.org/img/wn/${icon[0]}@2x.png`,
                    }}
                  />
                  <Text style={s.weather}>{weather.map(item => strUcFirst(item.description))}</Text>
                  <Title style={s.title}>{data.name}, {sys.country}</Title>
                  <Text style={s.currentDegree}>{parseInt(main.temp)} °{valueDegree}</Text>
                  <View style={s.min_max}>
                    <View style={s.temp}>
                      <Text style={s.txt_min_max}>Min</Text>
                      <Text style={s.deg_min_max}>{parseInt(main.temp_min).toFixed(1)} °{valueDegree}</Text>
                    </View>
                    <View style={s.temp}>
                      <Text style={s.txt_min_max}>Max</Text>
                      <Text style={s.deg_min_max}>{parseInt(main.temp_max).toFixed(1)} °{valueDegree}</Text>
                    </View>
                  </View>
                </View>
                <View style={[{ backgroundColor: context.bgOthers }, s.bloc_others]}>
                  <View style={[{ borderColor: context.color2 }, s.choose_button]} >
                    <TouchableOpacity onPress={() => setDisplay("today")} underlayColor="white" style={s.button}>
                      <View style={display === "today" ? [{ borderColor: context.buttonHome }, s.viewOn] : [{ borderColor: context.buttonHome, backgroundColor: context.buttonHome }, s.viewOff]}>
                        <Text style={display === "today" ? [{ color: context.colorHome }, s.on] : [{ color: "#FFFFFF" }, s.off]}>Aujourd'hui</Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setDisplay("week")} underlayColor="white" style={s.button}>
                      <View style={display === "week" ? [{ borderColor: context.buttonHome }, s.viewOn] : [{ borderColor: context.buttonHome, backgroundColor: context.buttonHome }, s.viewOff]}>
                        <Text style={display === "week" ? [{ color: context.colorHome }, s.on] : [{ color: "#FFFFFF" }, s.off]}>Cette semaine</Text>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => setDisplay("all")} underlayColor="white" style={s.button}>
                      <View style={display === "all" ? [{ borderColor: context.buttonHome }, s.viewOn] : [{ borderColor: context.buttonHome, backgroundColor: context.buttonHome }, s.viewOff]}>
                        <Text style={display === "all" ? [{ color: context.colorHome }, s.on] : [{ color: "#FFFFFF" }, s.off]}>Tout</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View>
                    {chooseDisplay()}
                  </View>
                  <Stats coord={coord} deg={degree} value={valueDegree} />
                </View>
              </View>
            }
          </View>
        </ScrollView >
      }
    </>
  );
}

const s = StyleSheet.create({
  image: {
    height: 25,
    width: 25
  },
  params: {
    top: 15,
    right: -150,
  },
  root: {
    width: windowWidth,
    height: "100%",
  },
  bloc_header: {
    height: 350,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    paddingTop: 15,
    paddingBottom: 15,
    fontSize: 25,
    color: "#fff",
  },
  currentDegree: {
    fontSize: 50,
    fontWeight: "700",
    textAlign: "center",
    marginBottom: 10,
    color: "white",
  },
  tinyLogo: {
    width: 120,
    height: 120,
  },
  min_max: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: 175,
  },
  temp: {
    display: "flex",
    alignItems: "center",
  },
  txt_min_max: {
    color: "white",
    fontSize: 20,
    fontWeight: "200"
  },
  deg_min_max: {
    color: "white",
    fontSize: 20,

  },
  bloc_others: {
    paddingTop: 20,
    marginTop: 30,
  },
  choose_button: {
    display: "flex",
    flexDirection: "row",
    paddingBottom: 15,
    borderBottomWidth: .5,
  },
  button: {
    marginLeft: 15,
  },
  viewOn: {
    padding: 7,
    borderRadius: 10,
    borderWidth: 1
  },
  viewOff: {
    padding: 7,
    borderRadius: 10,
    borderWidth: 1
  },
  on: {
    fontSize: 17,
    fontWeight: "600",
  },
  off: {
    fontSize: 17,
  },
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: 720,
  },
  loading_txt: {
    fontSize: 20,
    fontWeight: "600",
  },
  weather: {
    fontSize: 18,
    color: "white",
    fontStyle: "italic",
  },
  containerStyle: {
    height: 40,
    width: 150,
  },
  itemStyle: {
    justifyContent: 'center',
  },
  labelStyle: {
    fontSize: 17,
    textAlign: "center"
  },
  selector: {
    zIndex: 1,
    marginTop: 15,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalView: {
    margin: 20,
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    borderRadius: 10,
    padding: 10,
    marginTop: 25,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    fontWeight: "400",
    fontStyle: "italic"
  }
});

