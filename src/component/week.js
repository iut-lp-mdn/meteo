import React, { useEffect, useState, useContext } from 'react';
import { Text, View, StyleSheet, Dimensions, FlatList } from 'react-native';
import { ThemeContext } from './utils/context/theme-context';
import { week } from './utils/api/week_api'
import AffichagePrevisionnel from './affichagePrevisionnel'
import axios from 'axios';

const windowWidth = Dimensions.get('window').width;

export default function Week(props) {
    const date = new Date()
    const [daily, setDaily] = useState([])
    const [data, setData] = useState([])
    const [loadingData, setLoadingData] = useState(true);
    const [context, setContext] = useContext(ThemeContext);

    useEffect(() => {
        async function getData() {
            setLoadingData(true)
            const result = await axios.get(`https://api.openweathermap.org/data/2.5/onecall?lat=${props.coord.lat}&lon=${props.coord.lon}&units=${props.deg}&exclude=current,minutely,hourly&appid=8c19af5d8320e75a3a5fbf562b144995`)
            setDaily(result.data.daily)
            setLoadingData(false)
        }
        getData();
    }, [props]);

    useEffect(() => {
        daily.splice(0, 1) // Supprésion de la première ligne du tableau, inutile ici, car on veut uniquement les jours qui suivent.
        const tab_week = []
        const tab_day = []

        // Retourne un tableau me permettant d'avoir les jours dans l'ordre qui suit.
        // Si je lance la fonction un mercredi, le tableau me renverra : Jeudi, Vendredi, Samedi, Dimanche, ...
        week.map(item => {
            if (item.id === date.getDay()) {
                week.filter(obj => { if (obj.id > item.id) tab_week.push(obj) })
                week.filter(obj => { if (obj.id <= item.id) tab_week.push(obj) })
            }
        })

        // Fusionne mon tableau week (./utils/api/weekApi) avec le tableau de données renvoyé depuis openweathermap.
        daily.map((donnee, index_donnee) => {
            tab_week.map((jour_semaine, index_jour_semaine) => {
                if (index_donnee === index_jour_semaine) tab_day.push({ jour_semaine, donnee })
            })
        })
        setData(tab_day)

    }, [daily])
    return (
        <View style={[{ borderColor: context.color2 }, s.week]}>
            <View>
                {loadingData
                    ?
                    <View style={s.loading}>
                        <Text style={[{ color: context.color2 }, s.loading_txt]}>Chargement de votre Météo ...</Text>
                    </View>
                    :
                    <FlatList
                        data={data}
                        renderItem={({ item }) =>
                            <AffichagePrevisionnel
                                time={item.jour_semaine}
                                donnee={item.donnee}
                                isWeek={true}
                                value={props.value}
                            />
                        }
                        keyExtractor={(item, index) => index.toString()}
                        initialNumToRender={7}
                    />
                }
            </View>
        </View>
    );
}

const s = StyleSheet.create({
    week: {
        width: windowWidth,
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomWidth: .5,
    },
    loading: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        height: 720,
    },
    loading_txt: {
        fontSize: 20,
        fontWeight: "600",
    },
});

