import React, { useEffect, useState, useContext } from 'react';
import { Text, View, StyleSheet, Dimensions } from 'react-native';
import { ThemeContext } from './utils/context/theme-context';
import axios from 'axios';


const windowWidth = Dimensions.get('window').width;

export default function Stats(props) {
    const [data, setData] = useState([])
    const [wind, setWind] = useState({});
    const [main, setMain] = useState({});
    const [sunrise, setSunrise] = useState();
    const [sunset, setSunset] = useState();
    const [context, setContext] = useContext(ThemeContext);

    const [loadingData, setLoadingData] = useState(true);

    useEffect(() => {
        async function getData() {
            setLoadingData(true)
            const result = await axios.get(`http://api.openweathermap.org/data/2.5/weather?lat=${props.coord.lat}&lon=${props.coord.lon}&units=${props.deg}&appid=8c19af5d8320e75a3a5fbf562b144995`)
            setData(result.data)
            setWind(result.data.wind)
            setMain(result.data.main)
            setSunrise(result.data.sys.sunrise)
            setSunset(result.data.sys.sunset)
            setLoadingData(false)
        }
        getData();
    }, [props]);

    function getSun(item) {
        // Renvoie les heures du coucher et de lever du soleil, sur le système horaire 24 h.
        var sec = item;
        var date = new Date(sec * 1000);
        var timestr = date.toLocaleString("en-GB", { hour12: false });
        var convertedTime = timestr.slice(12, 17);

        return convertedTime
    }

    return (
        <View style={s.stat}>
            {loadingData
                ?
                <View style={s.loading}>
                    <Text style={[{ color: context.color2 }, s.loading_txt]}>Chargement de votre Météo ...</Text>
                </View>
                :
                <View>
                    <Text style={[{ color: context.color2 }, s.title]}>Statistiques</Text>
                    <View style={s.bloc}>
                        <View style={s.subBloc}>
                            <Text style={[{ color: context.color2 }, s.txt]}>Température Min</Text>
                            <Text style={[{ color: context.color2 }, s.data]}>{parseInt(Math.round(main.temp_min))} °{props.value}</Text>
                        </View>
                        <View style={s.subBloc}>
                            <Text style={[{ color: context.color2 }, s.txt]}>Température Max</Text>
                            <Text style={[{ color: context.color2 }, s.data]}>{parseInt(Math.round(main.temp_max))} °{props.value}</Text>
                        </View>
                    </View>
                    <View style={s.bloc}>
                        <View style={s.subBloc}>
                            <Text style={[{ color: context.color2 }, s.txt]}>Lever du soleil</Text>
                            <Text style={[{ color: context.color2 }, s.data]}>{getSun(sunrise)}</Text>
                        </View>
                        <View style={s.subBloc}>
                            <Text style={[{ color: context.color2 }, s.txt]}>Coucher du soleil</Text>
                            <Text style={[{ color: context.color2 }, s.data]}>{getSun(sunset)}</Text>
                        </View>
                    </View>
                    <View style={s.bloc}>
                        <View style={s.subBloc}>
                            <Text style={[{ color: context.color2 }, s.txt]}>Vitesse du vent</Text>
                            <Text style={[{ color: context.color2 }, s.data]}>{((wind.speed) * 3.6).toFixed(2)} km/h</Text>
                        </View>
                        <View style={s.subBloc}>
                            <Text style={[{ color: context.color2 }, s.txt]}>Degré du vent</Text>
                            <Text style={[{ color: context.color2 }, s.data]}>{wind.deg}°</Text>
                        </View>
                    </View>
                    <View style={s.bloc}>
                        <View style={s.subBloc}>
                            <Text style={[{ color: context.color2 }, s.txt]}>Humidité</Text>
                            <Text style={[{ color: context.color2 }, s.data]}>{main.humidity} %</Text>
                        </View>
                        <View style={s.subBloc}>
                            <Text style={[{ color: context.color2 }, s.txt]}>Préssion</Text>
                            <Text style={[{ color: context.color2 }, s.data]}>{main.pressure} hPa</Text>
                        </View>
                    </View>
                    <View style={s.bloc}>
                        <View style={s.subBloc}>
                            <Text style={[{ color: context.color2 }, s.txt]}>Visibilité</Text>
                            <Text style={[{ color: context.color2 }, s.data]}>{(data.visibility / 1000).toFixed(1)} km</Text>
                        </View>
                        <View style={s.subBloc}>
                            <Text style={[{ color: context.color2 }, s.txt]}>Nuages</Text>
                            <Text style={[{ color: context.color2 }, s.data]}>{data.clouds.all} %</Text>
                        </View>
                    </View>
                </View>
            }
        </View>
    );
}

const s = StyleSheet.create({
    stat: {
        width: windowWidth,
        marginTop: 20,
        paddingTop: 10,
        paddingBottom: 50,
    },
    title: {
        fontSize: 20,
        marginBottom: 10,
        marginLeft: 15
    },
    bloc: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
        paddingVertical: 10,
        marginTop: 10,
    },
    subBloc: {
        width: "50%",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "space-around",
        paddingTop: 5,
    },
    txt: {
        fontSize: 15,
        textAlign: "center",
        display: "flex",
        paddingBottom: 10,
        fontStyle: "italic",
        fontWeight: "200",
    },
    data: {
        fontSize: 15,
        textAlign: "center",
        display: "flex",
        fontWeight: "600",
    },
    loading: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        height: 720,
    },
    loading_txt: {
        fontSize: 20,
        fontWeight: "600",
    },
});

