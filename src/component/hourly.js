import React, { useEffect, useState, useContext } from 'react';
import { Text, View, StyleSheet, Dimensions, FlatList } from 'react-native';
import { ThemeContext } from './utils/context/theme-context';

import axios from 'axios';

import AffichagePrevisionnel from './affichagePrevisionnel'
import { hour } from './utils/api/hour_api'

const windowWidth = Dimensions.get('window').width;

export default function Hourly(props) {
    const date = new Date()
    const [hourly, setHourly] = useState([])
    const [data, setData] = useState([])
    const [context, setContext] = useContext(ThemeContext);

    const [loadingData, setLoadingData] = useState(true);

    useEffect(() => {
        async function getData() {
            setLoadingData(true)
            const result = await axios.get(`https://api.openweathermap.org/data/2.5/onecall?lat=${props.coord.lat}&lon=${props.coord.lon}&units=${props.deg}&exclude=current,minutely,daily&appid=8c19af5d8320e75a3a5fbf562b144995`)
            setHourly(result.data.hourly)
            setLoadingData(false)
        }
        getData();
    }, [props]);

    useEffect(() => {
        const tab_hour = []
        const tab = []

        // Retourne un tableau me permettant d'avoir les heures dans l'ordre qui suit.
        // Si je lance la fonction à 21h, le tableau me renverra : 22h, 23h, 00h, 01h, ...
        hour.map(item => {
            if (item === date.getHours()) { // on récupère la valeur de l'heure actuel.
                hour.filter(obj => { if (obj > item) tab_hour.push(obj < 10 ? obj = "0" + obj : obj) }) // On push les heures qui suivent pour avoir un tableau logique, exemple : 14h, 15h...
                hour.filter(obj => { if (obj <= item) tab_hour.push(obj < 10 ? obj = "0" + obj : obj) }) // Si on dépasse les 00h, on récupère alors les valeurs 01h, 02h, après celle déjà insérer.
            }
        })

        // Fusionne mon tableau hour (./utils/api/hourApi) avec le tableau de données renvoyé depuis openweathermap.
        hourly.map((donnee, index_donnee) => {
            tab_hour.map((heure, index_heure) => {
                if (index_donnee === index_heure) tab.push({ heure, donnee })
            })
        })

        setData(tab)

    }, [hourly])

    return (
        <View style={[{ borderColor: context.color2 }, s.hour]}>
            {loadingData
                ?
                <View style={s.loading}>
                    <Text style={[{ color: context.color2 }, s.loading_txt]}>Chargement de votre Météo ...</Text>
                </View>
                :
                <FlatList
                    indicatorStyle="black"
                    style={s.flatlist}
                    horizontal
                    data={data}
                    renderItem={({ item }) =>
                        <AffichagePrevisionnel
                            time={item.heure}
                            donnee={item.donnee}
                            isWeek={false}
                            value={props.value}
                        />
                    }
                    keyExtractor={(item, index) => index.toString()}
                    initialNumToRender={24}
                />
            }
        </View>
    );
}

const s = StyleSheet.create({
    hour: {
        width: windowWidth,
        paddingTop: 15,
        paddingBottom: 15,
        borderBottomWidth: .5,
    },
    flatlist: {
        paddingTop: 10,
        paddingBottom: 10,
    },
    loading: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        height: 720,
    },
    loading_txt: {
        fontSize: 20,
        fontWeight: "600",
    },
});

