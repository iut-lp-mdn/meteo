import React, { useState, useEffect, useContext } from 'react'
import { StyleSheet, Text, Image, View } from 'react-native';
import { ThemeContext } from './utils/context/theme-context';



export default function AffichagePrevisionnel({ time, donnee, isWeek, value }) {
    const [icon, setIcon] = useState([]);
    const [context, setContext] = useContext(ThemeContext);

    useEffect(() => {
        setIcon(donnee.weather.map(obj => obj.icon))
    }, [time, donnee]);
    return (
        <View style={s.bloc}>
            {isWeek
                ? // Renvoie le bloc pour la semaine, en fonction des variables envoyées.
                <View style={s.shadow}>
                    <View style={[{ backgroundColor: context.bgWeekly }, s.raw]}>
                        <Text style={[{ color: context.color2 }, s.day]}>{time.name}</Text>
                        <View style={s.logoWeek}>
                            <Image
                                style={s.tinyLogo}
                                source={{
                                    uri: `http://openweathermap.org/img/wn/${icon[0]}@2x.png`,
                                }}
                            />
                        </View>
                        <View style={s.weekDegree}>
                            <Text style={[{ color: context.color2 }, s.minDegree]}>{Number(donnee.temp.min).toFixed(1)} °{value}</Text>
                            <Text style={[{ color: context.color2 }, s.degree]}>|</Text>
                            <Text style={[{ color: context.color2 }, s.maxDegree]}>{Number(donnee.temp.max).toFixed(1)} °{value}</Text>
                        </View>
                    </View>
                </View>
                : // Sinon, renvoie le bloc pour les données journalières.
                <View style={[{ backgroundColor: context.bgHourly }, s.column]}>
                    <Text style={s.hour}>{time} h</Text>
                    <Text style={s.hourDegreee}>{Math.round(donnee.temp)} °{value}</Text>
                    <View style={s.logoHour}>
                        <Image
                            style={s.tinyLogo}
                            source={{
                                uri: `http://openweathermap.org/img/wn/${icon[0]}@2x.png`,
                            }}
                        />
                    </View>
                </View>
            }
        </View>
    )
}

const s = StyleSheet.create({
    bloc: {
        flex: 1,
    },
    logoWeek: {
        display: "flex",
        alignItems: "center",
        alignSelf: "center",

    },
    tinyLogo: {
        width: 50,
        height: 50,
    },
    logoHour: {
        display: "flex",
        alignItems: "center",
        alignSelf: "center",
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        marginVertical: 10,
    },
    raw: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-around",
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 20,
    },
    day: {
        fontSize: 17,
        fontStyle: "italic",
        width: "33%",
        textAlign: "center",
    },
    degree: {
        fontSize: 15,
        marginHorizontal: 10,
        textAlign: "center",
        width: "10%",
    },
    maxDegree: {
        fontSize: 15,
        textAlign: "center",
        fontWeight: "500",
    },
    minDegree: {
        fontSize: 15,
        textAlign: "center",
        fontWeight: "300",
    },
    column: {
        flex: 1,
        width: 140,
        height: 120,
        padding: 10,
        marginLeft: 10,
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "space-between",
    },
    hour: {
        fontSize: 17,
        color: "white",
        fontStyle: "italic",
    },
    hourDegreee: {
        marginTop: 15,
        textAlign: "center",
        fontSize: 17,
        width: 100,
        fontWeight: "700",
        color: "white"
    },
    weekDegree: {
        justifyContent: "space-around",
        flexDirection: "row",
        width: "33%",
    },
})