import React from "react";
import { Appearance } from "react-native"

// Ne fonctionne pas si l'on débug avec chrome : https://reactnative.dev/docs/appearance#getcolorscheme
const colorScheme = Appearance.getColorScheme()

export const themes = {
    light: {
        bgHome: "#ffffff",
        colorHome: "#186184",
        buttonHome: "#186184",
        searchBoxHome: "#186184",
        colorSearchBoxHome: "#186184",
        bgMeteoHeader: "#186184",
        colorMeteoHeader: "#ffffff",
        menu: "#186184",
        drawerMenuColor: "#A9A9A9",
        background: "#186184",
        bgOthers: "#ffffff",
        bgHourly: "#186184",
        bgWeekly: "#ffffff",
        color3: "#ffffff",
        color2: "#186184",
        color: "#000000",
        name: 'Light'
    },
    dark: {
        bgHome: "#332F2C",
        colorHome: "#ffffff",
        buttonHome: "#000000",
        searchBoxHome: "#000000",
        colorSearchBoxHome: "#ffffff",
        bgMeteoHeader: "#332F2C",
        colorMeteoHeader: "#ffffff",
        menu: "#000000",
        drawerMenuColor: "#ffffff",
        background: "#000000",
        bgOthers: "#332F2C",
        bgHourly: "#000000",
        bgWeekly: "#000000",
        color3: "#332F2C",
        color2: "#ffffff",
        color: "#ffffff",
        name: 'Dark'
    },
};

export const ThemeContext = React.createContext(colorScheme === "light" ? themes.light : themes.dark);
