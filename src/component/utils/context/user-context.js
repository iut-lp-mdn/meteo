import React from 'react'

export const users = [
    {
        id: 1,
        email: "johndoe@gmail.io",
        pseudo: "John Doe",
        password: "johndoe",
        userToken: "tokenJD"
    },
    {
        id: 2,
        email: "demo@gmail.com",
        pseudo: "demo",
        password: "demo",
        userToken: "tokenDemo"
    }
]

export const AuthContext = React.createContext();