export const week = [
    {
        id: 0, // Dimanche est à 0 pour coordonner avec le date.getDay() qui return le dimanche à 0
        name: "Dimanche"
    },
    {
        id: 1,
        name: "Lundi"
    }, 
    {
        id: 2,
        name: "Mardi"
    }, 
    {
        id: 3,
        name: "Mercredi"
    }, 
    {
        id: 4,
        name: "Jeudi"
    }, 
    {
        id: 5,
        name: "Vendredi"
    }, 
    {
        id: 6,
        name: "Samedi"
    }, 
]