export const degreeTab = [
    {
        label: "Kelvin",
        value: "standard"
    },
    {
        label: "Celsius",
        value: "metric"
    },
    {
        label: "Fahrenheit",
        value: "imperial"
    },
]