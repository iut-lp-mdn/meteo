import React, { useState, useContext, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import {
    Avatar,
    Title,
    Caption,
    Drawer,
    Text,
    Switch
} from 'react-native-paper';
import {
    DrawerContentScrollView,
    DrawerItem
} from '@react-navigation/drawer';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import cross from './utils/icon/cross.png'
import PP from './utils/icon/pp.png'

import { AuthContext, users } from './utils/context/user-context'
import { ThemeContext, themes } from './utils/context/theme-context';

export function DrawerContent(props) {
    const [isEnabled, setIsEnabled] = useState(true);
    const [context, setContext] = useContext(ThemeContext);
    const [userToken, setUserToken] = useState(undefined);
    const [userEmail, setUserEmail] = useState("Non connecté");
    const [userPseudo, setUserPseudo] = useState("Non connecté");
    const [loadingData, setLoadingData] = useState(true);

    function toggleTheme() {
        context.name === "Dark" ? setContext(themes.light) : setContext(themes.dark)
        setIsEnabled(previousState => !previousState);
    }

    const { signOut, getUsers } = useContext(AuthContext);

    useEffect(() => {
        async function getData() {
            setLoadingData(true);
            const userData = await getUsers();
            setUserToken(userData);
            makeUser()
            setLoadingData(false)
        }
        getData();
    }, [userToken])

    function makeUser() {
        if (userToken !== undefined) {
            users.map(item => {
                if (item.userToken === userToken) {
                    setUserEmail(item.email)
                    setUserPseudo(item.pseudo)
                }
            })
        }
    }

    return (
        <View style={[{ backgroundColor: context.bgHome }, s.bloc]}>
            <DrawerContentScrollView {...props}>
                <View style={s.drawerContent}>
                    <View style={s.userInfoSection}>
                        <Avatar.Image
                            source={PP}
                            size={50}
                        />
                        {!loadingData &&
                            <View style={{ marginLeft: 15, flexDirection: 'column' }}>
                                <Title style={[{ color: context.color }, s.title]}>{userEmail}</Title>
                                <Caption style={[{ color: context.color }, s.text]}>{userPseudo}</Caption>
                            </View>
                        }
                    </View>
                    <View style={s.drawerSection}>
                        <View style={s.blocTitle}>
                            <Text style={[{ color: context.drawerMenuColor }, s.title]}>Menu</Text>
                        </View>
                        <DrawerItem
                            icon={({ color, size }) => (
                                <View style={s.bgImage}>
                                    <Icon
                                        name="home-outline"
                                        color={color}
                                        size={size}
                                    />
                                </View>
                            )}
                            label="Accueil"
                            onPress={() => { props.navigation.navigate('Accueil') }}
                            labelStyle={[{ color: context.drawerMenuColor }, s.text]}
                        />
                        <DrawerItem
                            icon={({ color, size }) => (
                                <View style={s.bgImage}>
                                    <Icon
                                        name="weather-cloudy"
                                        color={color}
                                        size={size}
                                    />
                                </View>
                            )}
                            label="Météo"
                            onPress={() => { props.navigation.navigate('Meteo') }}
                            labelStyle={[{ color: context.drawerMenuColor }, s.text]}
                        />
                        <DrawerItem
                            icon={({ color, size }) => (
                                <View style={s.bgImage}>
                                    <Icon
                                        name="exit-to-app"
                                        color={color}
                                        size={size}
                                    />
                                </View>
                            )}
                            label="Se déconnecter"
                            onPress={() => { signOut() }}
                            labelStyle={[{ color: context.drawerMenuColor }, s.text]}
                        />
                    </View>
                    <View style={s.bottomDrawerSection}>
                        <View style={s.blocTitle}>
                            <Text style={[{ color: context.drawerMenuColor }, s.title]}>Préférences</Text>
                        </View>
                        <View style={s.drawerTheme}>
                            <Text style={{ color: context.color }}>Thème </Text>
                            <Switch
                                style={s.switch}
                                trackColor={{ false: "#000000", true: "#eeeeee" }}
                                thumbColor={isEnabled ? "#f5dd4b" : "#eeeeee"}
                                ios_backgroundColor="#3e3e3e"
                                onValueChange={toggleTheme}
                                value={isEnabled}
                            />
                        </View>
                    </View>
                </View>
            </DrawerContentScrollView>
            <View style={s.close}>
                <View style={s.shadow}>
                    <TouchableOpacity style={s.bloc_close} onPress={() => props.navigation.closeDrawer()}>
                        <Image source={cross} style={s.image} />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

const s = StyleSheet.create({
    bloc: {
        flex: 1,
    },
    drawerContent: {
        flex: 1,
    },
    title: {
        fontSize: 16,
        marginTop: 3,
        fontWeight: 'bold',
    },
    drawerSection: {
        marginTop: 30,
        padding: 7,
        borderColor: '#f4f4f4',
        borderTopWidth: 1,
        borderBottomWidth: 1
    },
    userInfoSection: {
        marginTop: 5,
        marginLeft: 10,
        marginBottom: 5,
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
    },
    drawerTheme: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16,
    },
    switch: {
        marginLeft: 30,
    },
    close: {
        height: "10%",
        justifyContent: "flex-end",
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
    },
    bloc_close: {
        bottom: 20,
        left: 20,
        height: 50,
        width: 50,
        borderRadius: 100,
        backgroundColor: "#E54F5F",
        justifyContent: "center",
        alignItems: "center",
    },
    image: {
        height: 25,
        width: 25
    },
    text: {
        fontSize: 15,
        fontWeight: "400"
    },
    bgImage: {
        backgroundColor: "white",
        borderRadius: 20,
        padding: 5,
    },
    bottomDrawerSection: {
        padding: 7,
        borderColor: '#f4f4f4',
        borderTopWidth: 1,
        borderBottomWidth: 1,
    },
    blocTitle: {
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 5,
    }
});
