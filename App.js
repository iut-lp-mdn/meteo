import React, { useState, useReducer, useMemo } from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'react-native';
import { DrawerContent } from './src/component/DrawerContent';
import { ThemeContext, themes } from './src/component/utils/context/theme-context'
import { AuthContext, users } from './src/component/utils/context/user-context';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Accueil from './src/pages/accueil';
import Meteo from './src/pages/meteo';
import RootNavigation from './src/pages/root';


StatusBar.setBarStyle('light-content')

const Drawer = createDrawerNavigator();

export default function App() {
  const [context, setContext] = useState(themes.light);

  const initialLoginState = {
    isLoading: true,
    userName: null,
    userToken: null,
  };

  const loginReducer = (prevState, action) => {
    switch (action.type) {
      case 'RETRIEVE_TOKEN':
        return {
          ...prevState,
          userToken: action.token,
          isLoading: false,
        };
      case 'LOGIN':
        return {
          ...prevState,
          userName: action.id,
          userToken: action.token,
          isLoading: false,
        };
      case 'LOGOUT':
        return {
          ...prevState,
          userName: null,
          userToken: null,
          isLoading: false,
        };
      case 'REGISTER':
        return {
          ...prevState,
          userName: action.id,
          userToken: action.token,
          isLoading: false,
        };
    }
  };

  const [loginState, dispatch] = useReducer(loginReducer, initialLoginState);

  const authContext = useMemo(() => ({
    signIn: async (foundUser) => {
      const userToken = String(foundUser[0].userToken);
      const userName = foundUser[0].pseudo;

      try {
        await AsyncStorage.setItem('userToken', userToken);
      } catch (e) {
        console.log(e);
      }
      dispatch({ type: 'LOGIN', id: userName, token: userToken });
    },
    signOut: async () => {
      try {
        await AsyncStorage.removeItem('userToken');
      } catch (e) {
        console.log(e);
      }
      dispatch({ type: 'LOGOUT' });
    },
    signUp: () => { },
    getUsers: async () => {
      try {
        const data = await AsyncStorage.getItem('userToken');
        return data;
      } catch (e) {
        console.log(e);
      }
    }
  }), []);

  return (
    <ThemeContext.Provider value={[context, setContext]}>
      <AuthContext.Provider value={authContext}>
        <NavigationContainer>
          {loginState.userToken !== null ? (
            <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}>
              <Drawer.Screen name="Accueil" component={Accueil} />
              <Drawer.Screen name="Meteo" component={Meteo} />
            </Drawer.Navigator>
          ) :
            <RootNavigation />
          }
        </NavigationContainer>
      </AuthContext.Provider>
    </ThemeContext.Provider>
  );
}
