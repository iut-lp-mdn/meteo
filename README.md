# Application Météo d'Yvan Thalamas

[![made with expo](https://img.shields.io/badge/MADE%20WITH%20EXPO-000.svg?style=for-the-badge&logo=expo&labelColor=4630eb&logoWidth=20)](https://github.com/expo/expo) [![supports iOS and Android](https://img.shields.io/badge/Platforms-Native-4630EB.svg?style=for-the-badge&logo=EXPO&labelColor=000&logoColor=fff)](https://github.com/expo/expo)

Application créer lors d'un projet de Licence professionnelle Métiers du numérique : conception, rédaction et réalisation web Parcours Production Multimédia, durant le cours de React native.


Pour installer le projet :
-

Il vous suffit de faire dans un terminal : 

- Via SSH :
`git clone git@gitlab.com:iut-lp-mdn/meteo.git`

- Via HTTPS :
`git clone https://gitlab.com/iut-lp-mdn/meteo.git`

Déplacez vous dans le fichier grâce à `cd /Meteo`

Ensuite installer les composants et librairies via :
`npm install` ou `yarn install`

Usage :
-


Maintenant que le projet est installé en local, il vous suffit d'éxécuter la command suivante :

`expo start` dans un terminal (dans le dossier Météo).

Cela vous ouvrira votre navigateur.

Vous devez sélectionner la case qui correspond à votre utilisation :
- Lan (si votre téléphone est sur la même connexion que votre ordinateur)
- Tunnel (si votre téléphone n'est pas sur la même connexion que votre ordinateur)

Et vous n'aurez plus qu'a scannez le QR Code.


